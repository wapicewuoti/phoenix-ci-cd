# Study: CI/CD pipeline for Elixir ecosystem
This study describes the setup of constant integration and deployment for Elixir-ecosystem apps. The setup of the framework is done using an Ubuntu 18.04 installation running in virtualbox. CI/CD is done using a Gitlab hosted agent.

## Used software
- Ubuntu 18.04
- Virtualbox
- Gitlab hosted runner
- Elixir
- Mix
- Phoenix framework


## Setup

### Installation of Elixir and Hex
The first package to install is Elixir. Ubuntu doesn't contain the repository for Elixir out-of-the-box, so the source has to be added manually:
```
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && sudo dpkg -i erlang-solutions_1.0_all.deb
```

The next step is to update the repository info of the package manager:
```
sudo apt update
```
and install Erlang, the language Elixir is based on:
```
sudo apt install esl-erlang
```

Next, install Elixir:
```
sudo apt install elixir
```
Before we can install the Phoenix Framework, a package manager for Elixir, Mix, has to be installed:

```
mix local.hex
```
This might not always work on Ubuntu systems. If it doesn't, Mix will be prompted to install while installing Phoenix framework anyway.

### Installation of Phoenix Framework
To install the Phoenix Framework (and Mix if it's not already installed), run
```
mix archive.install hex phx_new 1.4.9
```

Nodejs also needs to be installed:
```
sudo apt install nodejs
```

Phoenix uses PostgreSQL as its database by default, so install that too:
```
sudo apt install PostgreSQL
```
An additional dependency of Phoenix on Linux is the inotify-tools-package:
```
sudo apt install inotify-tools
```

### Setup of Phoenix app
The next step is creating the actual app using a Mix Task:
```
mix phx.new deployable
```

Also allow mix to automatically fetch all dependencies:
```
Fetch and install dependencies? [Yn] y
```
Here *deployable* is the app name which will be used throughout this guide. To substitute for your app, replace *deployable* and *Deployable* from all configuration files used in this guide.

Now, the created project folder can be added to git.

The next step is to configure the database to be used on GitLab's hosted runners. Add the following configuration to the file config/mix.exs:
```
# Configure your database
config :deployable, Deployable.Repo,
  username: "postgres",
  password: "postgres",
  database: "deployable_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
```
This info is used to find the database used on the hosted Gitlab runner. After pushing these modifications to GitLab, the app is ready for CI setup.

### Setup of GitLab CI
To configure CI in GitLab, create a gitlab-ci.yml in the project directory. Add the following contents to the file:
```
stages:
  - test

image: bitwalker/alpine-elixir-phoenix:latest

test:
  stage: test
  services:
    - postgres:latest
  variables:
    POSTGRES_HOST: postgres
    POSTGRES_USER: postgres # must match config/test.exs
    POSTGRES_PASSWORD: postgres # must match config/test.exs
    MIX_ENV: "test"
  script:
    - mix deps.get --only test
    - mix ecto.create
    - mix ecto.migrate
    - mix test
  only:
    - master
    - merge_requests
```
This file tells Gitlab to initialize a PostgreSQL database in its hosted runner. PostgreSQL username and password are defined based on the `POSTGRES_USER` and `POSTGRES_PASSWORD` variables. These have to be the same as defined on the Mix side in config/text.exs.

After pushing the changes to the repository, a pipeline should run.

![test pipeline](img/test_pipeline.png "Test pipeline")

After this the app is ready for deployment setup.

### Setup of Gigalixir for CD
Deployment is done to Gigalixir, which is installed via pip. To install pip, run:

```
sudo apt install python-pip
```

after pip has been installed, run 

```
sudo pip install gigalixir
```

and signup with

```
gigalixir signup
```
After creating your account, verify that it's working by running 

```
gigalixir login
```

and

```
gigalixir account
```

When logging in, remember to save your API key when prompted:
```
Would you like us to save your api key to your ~/.netrc file? [Y/n]: y
```

Next, change to your project's root directory to create the application Gigalixir:
```
gigalixir create -n deployable
gigalixir pg:create --free
```

The phoenix project needs a few modifications to be deployable, however.
First, open the file `config/prod.exs` and remove the following line:

```
import_config "prod.secret.exs"
```

then, modify the option `url` include the correct hostname for the application:

```
url: [host: "deployable.gigalixirapp.com", port: 80],
```

Next, add the `secret_key_base` to use the automatically generated environment variable provided by Gigalixir.

```
secret_key_base: Map.fetch!(System.get_env(), "SECRET_KEY_BASE"),
```

Last, set `server` to true.

```
server: true
```

The configuration should look like the following:

```
config :deployable, Deployable.Endpoint,
  http: [:inet6, port: System.get_env("PORT") || 4000],
  url: [host: "deployable.gigalixirapp.com", port: 80],
  cache_static_manifest: "priv/static/cache_manifest.json",
  secret_key_base: Map.fetch!(System.get_env(), "SECRET_KEY_BASE"),
  server: true
```

Gigalixir also needs database information. To use PostgreSQL, add the following to `config/prod.exs`:
```
config :deployable, Deployable.Repo,
  adapter: Ecto.Adapters.Postgres,
  url: System.get_env("DATABASE_URL"),
  ssl: true,
  pool_size: 2
```

To deploy the app correclty, Elixir and Erlang version information must be included. Create a file `elixir_buildpack.config` in the project parent directory. Then, add version info based on the output of the command `elixir --version`. On this setup, the command returns the following:

```
Elixir 1.9.1 (compiled with Erlang/OTP 21)
```

Based on the output, the file contents would be the following:

```
elixir_version=1.9.1
erlang_version=21.0
```

When using Phoenix version 1.4 or higher, webpack needs to be enabled. To enable it, a `compile`-file must be found in the parent directory. Create it and add the following contents to it:

```
npm run deploy
cd $phoenix_dir
mix "${phoenix_ex}.digest"
```

The application is now ready to be deployed into Gigalixir. Commit the changes and run:

```
git push gigalixir master
```

Check that that the app was deployed succesfully:

```
gigalirix open
```

### Setup GitLab for CD
The next step is to setup automatic deploying from Gitlab. To achieve this, an environment variable *GIGALIXIR_REMOTE_URL* has to be set in Gitlab's CI/CD settings. The variable can be generated based on the information in the file `~/.netrc`. The file should contain an entry for an username and a password under `machine api.gigalixir.com`.

![gitlab_ci_settings](img/pipeline_variables.png)

The syntax for the value is the following:
```
https://username:password@git.gigalixir.com/appname.git
```
Again, remember to substitute *appname* with the name of your app.

NOTE: the @-symbol in `login` has to be inputted as "%40".

The next step is updating .gitlab-ci.yml to include deployment steps to Gigalixir.
Add a new stage called `deploy` after the 'test' stage. The lines to add are shown below with a `+`-sign:

```
stages:
  - test
+  - deploy

image: bitwalker/alpine-elixir-phoenix:latest

test:
  stage: test
  services:
    - postgres:latest
  variables:
    POSTGRES_HOST: postgres
    POSTGRES_USER: postgres # must match config/test.exs
    POSTGRES_PASSWORD: postgres # must match config/test.exs
    MIX_ENV: "test"
  script:
    - mix deps.get --only test
    - mix ecto.create
    - mix ecto.migrate
    - mix test
  only:
    - master
    - merge_requests

+ deploy: 
+   stage: deploy
+   script:
+     - git remote add gigalixir $GIGALIXIR_REMOTE_URL
+     - git push -f gigalixir HEAD:refs/heads/master
+   only:
+     - master
```
NOTE: remove the `+`-signs from the actual configuration file!

One optional step in deployment is to run migrations automatically when deploying. To do this, add a `Procfile` to the root directory with the following contents:

```
web: mix ecto.migrate && elixir --name $MY_NODE_NAME --cookie
$MY_COOKIE -S mix phx.server
```

After pushing the changes to Gitlab, the pipeline should deploy the app after running the tests:

![CI/CD pipeline](/img/pipeline.png "Pipelines running")

Now the CD configuration is ready.
