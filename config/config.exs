# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :deployable,
  ecto_repos: [Deployable.Repo]

# Configures the endpoint
config :deployable, DeployableWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "s0vWRsnUKuAW8g9dyGHKwyHc6q+eco96+XzSt27Q7w3HGJWVcy1KLRNZ+k2k66PB",
  render_errors: [view: DeployableWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Deployable.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
