defmodule Deployable.Repo do
  use Ecto.Repo,
    otp_app: :deployable,
    adapter: Ecto.Adapters.Postgres
end
