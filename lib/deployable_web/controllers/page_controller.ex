defmodule DeployableWeb.PageController do
  use DeployableWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
